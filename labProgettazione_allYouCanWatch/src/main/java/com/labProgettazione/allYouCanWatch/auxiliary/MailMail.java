package com.labProgettazione.allYouCanWatch.auxiliary;
import org.springframework.mail.MailSender;  
import org.springframework.mail.SimpleMailMessage;  

// classe per invio mail
public class MailMail{  
    private MailSender mailSender;  
   
    public void setMailSender(MailSender mailSender) {  
        this.mailSender = mailSender;  
    }  
   
    public void sendMail(String from, String to, String subject, String msg) {  
        
    	//creazione messaggio  
        SimpleMailMessage message = new SimpleMailMessage();  
        message.setFrom(from);  
        message.setTo(to);  
        message.setSubject(subject);  
        message.setText(msg);
    
        //invio messaggio  
        mailSender.send(message);
    }  
}  