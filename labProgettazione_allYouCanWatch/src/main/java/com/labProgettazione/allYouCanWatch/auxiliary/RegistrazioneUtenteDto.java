package com.labProgettazione.allYouCanWatch.auxiliary;

// classe di supporto alla registrazione utente dto (oggetto di trasferimento dati)
public class RegistrazioneUtenteDto {
	
	//attributi classe
    private String nome;
    private String cognome;
    private String password;
    private String email;
    private String confermaPassword;
    
    //costruttori classe
    public RegistrazioneUtenteDto () {
    }
    
    public RegistrazioneUtenteDto(String nome, String cognome, String password, String confermaPassword, String email) {
		super();
		this.nome = nome;
		this.cognome = cognome;
		this.password = password;
		this.email = email;
		this.confermaPassword = confermaPassword;
	}

    // metodi classe
	public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfermaPassword() {
		return confermaPassword;
	}
	public void setConfermaPassword(String confermaPassword) {
		this.confermaPassword = confermaPassword;
	}
	public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}