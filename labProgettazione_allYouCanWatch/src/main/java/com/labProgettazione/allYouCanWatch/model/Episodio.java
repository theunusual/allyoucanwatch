package com.labProgettazione.allYouCanWatch.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

//questa classe rappresenta l'entità episodio
@Entity
@Table(name= "episodio")
public class Episodio {
	
	//attributi della classe
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "numero")
	private int numero;
	
	@Column(name = "titolo")
	private String titolo;

	@Column(name = "trama",  columnDefinition = "LONGTEXT")
	private String trama;

	@Column(name = "durata_min")
	private int durata_min;

	@Column(name = "ascolti")
	private int ascolti;
	
	//mapping della relazione many to many
	@ManyToOne
	@JoinColumn(name = "stagione_id")
	private Stagione stagione;
	
	//costruttore
	public Episodio() {
		super();
	}
	
	//costruttore parametrizzato
	public Episodio(Long id, int numero, String titolo, String trama, int durata_min, int ascolti, Stagione stagione) {
		super();
		this.id = id;
		this.numero = numero;
		this.titolo = titolo;
		this.trama = trama;
		this.durata_min = durata_min;
		this.ascolti = ascolti;
		this.stagione = stagione;
	}

	//metodi della classe
	
	public Long getId() {
		return this.id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public int getNumero() {
		return this.numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getTitolo() {
		return this.titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

	public String getTrama() {
		return this.trama;
	}

	public void setTrama(String trama) {
		this.trama = trama;
	}

	public int getDurata_min() {
		return this.durata_min;
	}

	public void setDurata_min(int durata_min) {
		this.durata_min = durata_min;
	}

	public int getAscolti() {
		return this.ascolti;
	}

	public void setAscolti(int ascolti) {
		this.ascolti = ascolti;
	}

	public Stagione getStagione() {
		return stagione;
	}

	public void setStagione(Stagione stagione) {
		this.stagione = stagione;
	}
	
	
	@Override
	public String toString() {
		return "Episodio [numero=" + numero + ", titolo=" + titolo + "]";
	}
	

}