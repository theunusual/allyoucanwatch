package com.labProgettazione.allYouCanWatch.model;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

//questa classe rappresenta l'entità stagione
@Entity
@Table(name= "stagione")
public class Stagione {
	
	//attributi della classe
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "numero")
	private int numero;
	
	@Column(name = "trama",  columnDefinition = "LONGTEXT")
	private String trama;

	@Column(name = "anno_uscita")
	private int anno_uscita;
	
	@OneToMany(mappedBy = "stagione" , fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Collection<Episodio> episodi;

	//mapping della relazione many to one
	@ManyToOne
	@JoinColumn(name = "serie_tv_id")
	private SerieTv serieTv;
	
	//costruttore
	public Stagione() {
		super();
	}

	//costruttore parametrizzato
	public Stagione(Long id, int numero, String trama, int anno_uscita, Collection<Episodio> episodi, SerieTv serieTv) {
		super();
		this.id = id;
		this.numero = numero;
		this.trama = trama;
		this.anno_uscita = anno_uscita;
		this.episodi = episodi;
		this.serieTv = serieTv;
	}


	//metodi della classe
	public Long getId() {
		return this.id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public int getNumero() {
		return this.numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getTrama() {
		return this.trama;
	}

	public void setTrama(String trama) {
		this.trama = trama;
	}

	public int getAnno_uscita() {
		return this.anno_uscita;
	}

	public void setAnno_uscita(int anno_uscita) {
		this.anno_uscita = anno_uscita;
	}

	public SerieTv getSerieTv() {
		return serieTv;
	}
	
	public void setSerieTv(SerieTv serieTv) {
		this.serieTv = serieTv;
	}

	public int getNumeroEpisodi() {
		return episodi.size();
	}
	
	public Episodio[] getEpisodi() {
		Episodio[] episodiArray = episodi.toArray(new Episodio [episodi.size()]);
		Episodio temp;
		for(int i = 0; i< episodiArray.length; i++) {
			for(int j = i+1; j<episodiArray.length; j++)
				if(episodiArray[i].getNumero() > episodiArray[j].getNumero()) {
					temp = episodiArray[i];
					episodiArray[i] = episodiArray[j];
					episodiArray[j] = temp;
				}
		}
		return episodiArray;
			
	}

}