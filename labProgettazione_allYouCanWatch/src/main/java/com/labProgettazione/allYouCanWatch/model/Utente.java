package com.labProgettazione.allYouCanWatch.model;

import javax.persistence.*;
import java.util.Collection;

//entità utente
@Entity
@Table(name = "utente", uniqueConstraints = @UniqueConstraint(columnNames = "email"))
public class Utente {
	
	//attributi classe
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(name = "nome")
    private String nome;
    
    @Column(name = "cognome")
    private String cognome;
    
    @Column(name = "email")
    private String email;
    
    @Column(name = "password")
    private String password;
    
    //relazione many to many con l'entità ruoli
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
        name = "utenti_ruoli",
        joinColumns = @JoinColumn(
            name = "id_utente", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(
            name = "id_ruolo", referencedColumnName = "id"))
    private Collection < Ruolo > ruoli;
    
    //costruttori classe
    public Utente() {}

    public Utente(String nome, String cognome, String email, String password) {
        this.nome = nome;
        this.cognome = cognome;
        this.email = email;
        this.password = password;
        

    }
    
    public Utente(String nome, String cognome, String email, String password, Collection < Ruolo > ruoli) {
        this.nome = nome;
        this.cognome = cognome;
        this.email = email;
        this.password = password;
        this.ruoli = ruoli;
    }
    
    //metodi della classe 
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return nome;
    }
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

	public Collection < Ruolo > getRuoli() {
        return ruoli;
    }

    public void setRuoli(Collection < Ruolo > ruoli) {
        this.ruoli = ruoli;
    }

    @Override
    public String toString() {
        return "Utente{" +
            "id=" + id +
            ", Nome='" + nome + '\'' +
            ", Cognome='" + cognome + '\'' +
            ", Email='" + email + '\'' +
            ", Password='" + "*********" + '\'' +
            ", Ruoli=" + ruoli +
            '}';
    }
}