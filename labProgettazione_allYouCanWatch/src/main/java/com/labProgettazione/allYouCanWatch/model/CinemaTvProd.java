package com.labProgettazione.allYouCanWatch.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;


@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name= "cinema_tv_prod")
public class CinemaTvProd {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected Long id;
	
	@Column(name = "titolo")
	private String titolo;
	
	@Column(name = "trama",  columnDefinition = "LONGTEXT")
	private String trama;

	@Column(name = "lingua_originale")
	private String lingua_originale;
	
	@Column(name = "genere")
	private String genere;
	
	@Column(name = "scenegg_originale")
	private boolean scenegg_originale;
	
	@Column(name = "tipologia_produzione")
	private String tipologia_produzione;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "prod_attori",
		joinColumns = {@JoinColumn(name = "prod_id")},
		inverseJoinColumns = {@JoinColumn(name = "attore_id")})
	private Set<Attore> attori = new HashSet<>();

	public CinemaTvProd() {
		super();
	}

	public CinemaTvProd(Long id, String titolo, String trama, String lingua_originale, String genere,
			boolean scenegg_originale) {
		this.id = id;
		this.titolo = titolo;
		this.trama = trama;
		this.lingua_originale = lingua_originale;
		this.genere = genere;
		this.scenegg_originale = scenegg_originale;
	}



	public Long getId() {
		return this.id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitolo() {
		return this.titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}
	
	public String getTipologia_produzione() {
		return tipologia_produzione;
	}

	public void setTipologia_produzione(String tipologia_produzione) {
		this.tipologia_produzione = tipologia_produzione;
	}

	public String getTrama() {
		return this.trama;
	}

	public void setTrama(String trama) {
		this.trama = trama;
	}

	public String getLingua_originale() {
		return this.lingua_originale;
	}

	public void setLingua_originale(String lingua_originale) {
		this.lingua_originale = lingua_originale;
	}

	public String getGenere() {
		return this.genere;
	}

	public void setGenere(String genere) {
		this.genere = genere;
	}

	public boolean isScenegg_originale() {
		return this.scenegg_originale;
	}

	public boolean getScenegg_originale() {
		return this.scenegg_originale;
	}

	public void setScenegg_originale(boolean scenegg_originale) {
		this.scenegg_originale = scenegg_originale;
	}

	public Set<Attore> getAttori() {
		return attori;
	}

	public void setAttori(Set<Attore> attori) {
		this.attori = attori;
	}
	
	public void addAttoreCast(Attore attore) {
		attore.addCinemaTvProd(this);
	}
	
	public void removeAttoreCast(Attore attore) {
		attore.getCinemaTvProd().remove(this);
	}
}
