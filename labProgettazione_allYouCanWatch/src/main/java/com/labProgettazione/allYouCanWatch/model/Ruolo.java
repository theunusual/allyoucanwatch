package com.labProgettazione.allYouCanWatch.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

//entità ruolo
@Entity
@Table(name = "ruolo")
public class Ruolo {
	
	//attributi classe
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;
    
    @Column(name = "nome")
    private String nome;
    
    //costruttori classe
    public Ruolo() {}

    public Ruolo (String nome) {
        this.nome = nome;
    }
    
    //metodi classe
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public String toString() {
        return "Ruolo{" +
            "Id=" + id +
            ", Nome='" + nome + '\'' +
            '}';
    }
}