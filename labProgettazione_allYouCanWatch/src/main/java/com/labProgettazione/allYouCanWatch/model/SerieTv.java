package com.labProgettazione.allYouCanWatch.model;

import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

//questa classe rappresenta l'entità serieTv
@Entity
@Table(name= "serie_tv")
public class SerieTv extends CinemaTvProd{
	
	//Attributi della classe
	@Column(name = "in_corso")
	private boolean in_corso;
	
	@Column(name = "titolo_sigla")
	private String titolo_sigla;
	
	@OneToMany(mappedBy="serieTv", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Collection<Stagione> stagioni; 
	
	@ManyToOne
	@JoinColumn(name = "spinOff_id")
	private SerieTv serieTvSpinOff;
	
	@OneToMany(mappedBy = "serieTvSpinOff", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Collection<SerieTv> derivati ; 
	
	//costruttore
	public SerieTv() {
		super();
		this.setTipologia_produzione("Serie");
	}

	//costruttore parametrizzato
	public SerieTv(boolean in_corso, String titolo_sigla, Long id, String titolo, String trama, String lingua_originale, String genere, boolean scenegg_originale, SerieTv serieTvSpinOff) {
		//this.id = id;
		super(id, titolo, trama, lingua_originale, genere, scenegg_originale);
		this.in_corso = in_corso;
		this.titolo_sigla = titolo_sigla;
		this.serieTvSpinOff = serieTvSpinOff;
		this.setTipologia_produzione("Serie");
	}

	

	//metodi della classe
	
	public boolean getIn_corso() {
		return this.in_corso;
	}

	public void setIn_corso(boolean in_corso) {
		this.in_corso = in_corso;
	}

	public String getTitolo_sigla() {
		return this.titolo_sigla;
	}

	public void setTitolo_sigla(String titolo_sigla) {
		this.titolo_sigla = titolo_sigla;
	}

	public SerieTv getSerieTvSpinOff() {
		return serieTvSpinOff;
	}

	public void setSerieTvSpinOff(SerieTv serieTvSpinOff) {
		this.serieTvSpinOff = serieTvSpinOff;
	}
	
	public Stagione[] getStagioni() {
		Stagione[] stagioniArray = stagioni.toArray(new Stagione[stagioni.size()]);
		Stagione temp;
		for(int i = 0; i< stagioniArray.length; i++) {
			for(int j = i+1; j<stagioniArray.length; j++)
				if(stagioniArray[i].getNumero() > stagioniArray[j].getNumero()) {
					temp = stagioniArray[i];
					stagioniArray[i] = stagioniArray[j]; 
					stagioniArray[j] = temp;

				}
		}
		return stagioniArray;

	}
	
	public int getNumeroStagioni() {
		return stagioni.size();
	}

	@Override
	public String toString() {
		return getTitolo();
	}
	
	
	
}