package com.labProgettazione.allYouCanWatch.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "film")
public class Film extends CinemaTvProd{
	
	//Attributi della classe
	@Column(name = "tipo")
	private String tipo;
	
	@Column(name = "durata_min")
	private int durata_min;
	
	@Column(name = "anno_uscita")
	private int anno_uscita;

	//costruttore
	public Film() {
		super();
		this.setTipologia_produzione("Film");
	}
	
	//costruttore parametrizzato
	public Film(Long id, String titolo,  String trama, String lingua_originale, String genere,
			boolean scenegg_originale, String tipo, int durata_min, int anno_uscita) {
		super(id, titolo, trama, lingua_originale, genere, scenegg_originale);
		this.tipo = tipo;
		this.durata_min = durata_min;
		this.anno_uscita = anno_uscita;
		this.setTipologia_produzione("Film");
	}
	
	//metodi get e set della classe

	public int getDurata_min() {
		return durata_min;
	}


	public void setDurata_min(int durata_min) {
		this.durata_min = durata_min;
	}

	public int getAnno_uscita() {
		return anno_uscita;
	}

	public void setAnno_uscita(int anno_uscita) {
		this.anno_uscita = anno_uscita;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

}
