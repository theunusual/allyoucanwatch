package com.labProgettazione.allYouCanWatch.model;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

//questa classe rappresenta l'entità attore con relativi metodi e attributi
@Entity
@Table(name= "attore")
public class Attore {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "nome")
	private String nome;
	
	@Column(name = "cognome")
	private String cognome;

	@Column(name = "sesso")
	private String sesso;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name = "data_nascita")
	private Date data_nascita;
	
	@Column(name = "stato_nascita")
	private String stato_nascita;
	
	//relazione many to many con cinemaTvProd
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "prod_attori",
		joinColumns = {@JoinColumn(name = "attore_id")},
		inverseJoinColumns = {@JoinColumn(name = "prod_id")})
	private Set<CinemaTvProd> cinemaTvProd = new HashSet<>();

	//costruttore
	public Attore() {
		super();
	}

	//costruttore parametrizzato
	public Attore(Long id, String nome, String cognome, String sesso, Date data_nascita, String stato_nascita) {
		this.id = id;
		this.nome = nome;
		this.cognome = cognome;
		this.sesso = sesso;
		this.data_nascita = data_nascita;
		this.stato_nascita = stato_nascita;
	}
	
	// serie di set e get su tutti gli attributi
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return this.nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return this.cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public String getSesso() {
		return this.sesso;
	}
	public void setSesso(String sesso) {
		this.sesso = sesso;
	}
	public Date getData_nascita() {
		return this.data_nascita;
	}
	public void setData_nascita(Date data_nascita) {
		this.data_nascita = data_nascita;
	}
	public String getStato_nascita() {
		return this.stato_nascita;
	}
	public void setStato_nascita(String stato_nascita) {
		this.stato_nascita = stato_nascita;
	}

	public Set<CinemaTvProd> getCinemaTvProd() {
		return cinemaTvProd;
	}

	public void setCinemaTvProd(Set<CinemaTvProd> cinemaTvProd) {
		for (CinemaTvProd cinemaTvProd2 : cinemaTvProd) {
			addCinemaTvProd(cinemaTvProd2);
		}	
	}
	
	public void addCinemaTvProd(CinemaTvProd cinemaTvProd) {
		cinemaTvProd.getAttori().add(this);
	}
	
	public void removeCinemaTvProd(CinemaTvProd cinemaTvProd) {
		cinemaTvProd.getAttori().remove(this);
	}

	@Override
	public String toString() {
		return "Attore [id=" + id + ", nome=" + nome + ", cognome=" + cognome + "]";
	}
}
