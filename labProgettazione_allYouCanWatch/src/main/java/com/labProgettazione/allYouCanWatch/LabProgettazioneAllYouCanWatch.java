package com.labProgettazione.allYouCanWatch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LabProgettazioneAllYouCanWatch {
	
	// esecuzione applicazione spring 
	public static void main(String[] args) {
		SpringApplication.run(LabProgettazioneAllYouCanWatch.class, args);
	}

}
