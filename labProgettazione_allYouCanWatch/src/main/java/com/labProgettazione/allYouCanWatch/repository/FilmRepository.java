package com.labProgettazione.allYouCanWatch.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.labProgettazione.allYouCanWatch.model.Film;

@Repository
public interface FilmRepository extends JpaRepository<Film, Long>{
	//ricerca nella stringa
	@Query(value = "SELECT * FROM FILM film JOIN CINEMA_TV_PROD prod ON film.ID = prod.ID WHERE prod.titolo iLIKE %?1%", nativeQuery=true)
	public List<Film> findFiltered(String paramTitolo);
	
	//ricerca partendo dall'inzio
	@Query(value = "SELECT * FROM FILM film JOIN CINEMA_TV_PROD prod ON film.ID = prod.ID WHERE prod.titolo iLIKE ?1%", nativeQuery=true)
	public List<Film> filtroTitolo(String paramTitolo);
}
