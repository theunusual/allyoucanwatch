package com.labProgettazione.allYouCanWatch.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.labProgettazione.allYouCanWatch.model.CinemaTvProd;


@Repository
public interface CinemaTvProdRepository extends JpaRepository<CinemaTvProd, Long>{
	//ricerca nella stringa
	@Query(value = "SELECT *, 0 AS clazz_ FROM CINEMA_TV_PROD prod WHERE prod.titolo iLIKE %?1%", nativeQuery=true)
	public List<CinemaTvProd> findFiltered(String paramTitolo);
	
	//ricerca partendo dall'inzio
	@Query(value = "SELECT *, 0 AS clazz_ FROM CINEMA_TV_PROD prod WHERE prod.titolo iLIKE ?1%", nativeQuery=true)
	public List<CinemaTvProd> filtroTitolo(String paramTitolo);
}
