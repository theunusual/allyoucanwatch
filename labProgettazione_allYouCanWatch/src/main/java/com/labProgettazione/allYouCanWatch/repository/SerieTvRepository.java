package com.labProgettazione.allYouCanWatch.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.labProgettazione.allYouCanWatch.model.SerieTv;
@Repository
public interface SerieTvRepository extends JpaRepository<SerieTv, Long>{
	
	@Query(value = "SELECT * FROM SERIE_TV ser JOIN CINEMA_TV_PROD prod ON ser.ID = prod.ID WHERE prod.titolo iLIKE %?1%", nativeQuery=true)
	public List<SerieTv> findFiltered(String paramTitolo);
	
	@Query(value = "SELECT * FROM SERIE_TV ser JOIN CINEMA_TV_PROD prod ON ser.ID = prod.ID WHERE prod.titolo iLIKE ?1%", nativeQuery=true)
	public List<SerieTv> filtroTitolo(String paramTitolo);

}
