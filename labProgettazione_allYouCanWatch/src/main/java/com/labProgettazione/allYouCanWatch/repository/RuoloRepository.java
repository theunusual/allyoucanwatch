package com.labProgettazione.allYouCanWatch.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.labProgettazione.allYouCanWatch.model.Ruolo;

//repository entità ruolo
@Repository
public interface RuoloRepository extends JpaRepository<Ruolo, Long> {
	
}
