package com.labProgettazione.allYouCanWatch.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.labProgettazione.allYouCanWatch.model.Attore;

@Repository
public interface AttoreRepository extends JpaRepository<Attore, Long> {
	
}
