package com.labProgettazione.allYouCanWatch.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.labProgettazione.allYouCanWatch.model.Stagione;
@Repository
public interface StagioneRepository extends JpaRepository<Stagione, Long>{
	

}
