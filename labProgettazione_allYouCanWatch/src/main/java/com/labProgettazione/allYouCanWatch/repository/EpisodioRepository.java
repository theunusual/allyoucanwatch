package com.labProgettazione.allYouCanWatch.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.labProgettazione.allYouCanWatch.model.Episodio;
@Repository
public interface EpisodioRepository extends JpaRepository<Episodio, Long>{
	

}
