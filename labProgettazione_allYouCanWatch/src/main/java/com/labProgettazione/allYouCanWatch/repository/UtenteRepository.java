package com.labProgettazione.allYouCanWatch.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.labProgettazione.allYouCanWatch.model.Utente;

//repository entità utente
@Repository
public interface UtenteRepository extends JpaRepository < Utente, Long > {
    //dichiarazione metodo per estrazione utente da mail
	Utente findByEmail(String email);

}