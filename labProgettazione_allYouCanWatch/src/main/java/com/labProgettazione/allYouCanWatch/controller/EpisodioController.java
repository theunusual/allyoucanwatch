package com.labProgettazione.allYouCanWatch.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.labProgettazione.allYouCanWatch.model.Episodio;
import com.labProgettazione.allYouCanWatch.model.Stagione;
import com.labProgettazione.allYouCanWatch.service.EntityService;

@Controller
public class EpisodioController {

	@Autowired
	private EntityService<Episodio> episodioService;
	
	@Autowired
	private EntityService<Stagione> stagioneService;
	
	@GetMapping("/newEpisodioForm/{id}")
	public String newEpisodioForm(@PathVariable (value = "id") Long id, Model model) {
		//creazione dell'attributo model per abbinare i dati del form
		Episodio episodio = new Episodio();
		model.addAttribute("episodio", episodio);
		model.addAttribute("stagione", stagioneService.getEntityById(id));
		return "newEpisodio";
	}
	
	//stampa della trama della stagione
	@GetMapping("/trama_episodio/{id_episodio}")
	public String tramaStagione(@PathVariable (value = "id_episodio") Long id, Model model) {
		//se si vuole vedere l'intera lista del DB basta implementare un if con id = 0 che indica tutte le stagioni
		Episodio episodio = (Episodio) episodioService.getEntityById(id);
		model.addAttribute("titolo", episodio.getTitolo());
		model.addAttribute("trama", episodio.getTrama());
		return "trama";
	}
	
	@PostMapping("/saveEpisodio")
	public String saveEpisodio(@ModelAttribute("episodio") Episodio episodio) {
		//salvo nel database
		episodioService.saveEntity(episodio);
		return "redirect:/episodi/" + episodio.getStagione().getId();
		
	}
	
	@GetMapping("updateEpisodio/{id}")
	public String updateEpisodio(@PathVariable (value = "id") Long id, Model model) {
		// get attore dal service
		Episodio episodio = (Episodio) episodioService.getEntityById(id);
		// set attore come un attiributo model da un form pre-popolato
		model.addAttribute("episodio", episodio);
		model.addAttribute("stagione", episodio.getStagione());
		return "updateEpisodio";
	}
	
	@GetMapping("/deleteEpisodio/{id}")
	public String deleteEpisodio(@PathVariable (value = "id") Long id) {
		//richiamo il metodo di cancellazione dell'attore
		Episodio episodio = (Episodio) episodioService.getEntityById(id);
		this.episodioService.deleteEntityById(id);
		return "redirect:/episodi/" + episodio.getStagione().getId();
	}
	
	//stampa degli episodi presenti nel database
	@GetMapping("/episodi/{id}")
	public String viewEpisodiPage(@PathVariable (value = "id") Long id, Model model){
		model.addAttribute("stagione", stagioneService.getEntityById(id));
		return "episodi";
	}
}
