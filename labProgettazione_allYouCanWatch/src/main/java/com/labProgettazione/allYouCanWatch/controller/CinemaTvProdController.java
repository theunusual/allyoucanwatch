package com.labProgettazione.allYouCanWatch.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.labProgettazione.allYouCanWatch.model.CinemaTvProd;
import com.labProgettazione.allYouCanWatch.service.CinemaTvProdServiceImpl;

@Controller
public class CinemaTvProdController {
	
	@Autowired
	private CinemaTvProdServiceImpl cinemaService;
	
	@GetMapping("cast/{id}")
	public String castProduzioneCinematografica(@PathVariable (value = "id") Long id, Model model) {
		// get cinematvprod dal service
		CinemaTvProd produzione = (CinemaTvProd) cinemaService.getEntityById(id);
		// set serieTv come un attiributo model da un form pre-popolato
		model.addAttribute("produzione", produzione);
		return "cast";
	}
	
	
	@GetMapping("trama/{id}")
	public String tramaProduzioneCinematografica(@PathVariable (value = "id") Long id, Model model) {
		// get cinemaTvProd dal service
		CinemaTvProd produzione = (CinemaTvProd) cinemaService.getEntityById(id);
		// set cinemaTvProd come un attiributo model da un form pre-popolato
		model.addAttribute("titolo", produzione.getTitolo());
		model.addAttribute("trama", produzione.getTrama());
		return "trama";
	}
	
	@GetMapping("/produzioni")
	public String viewFilmPage(Model model){
		model.addAttribute("listProduzioni", cinemaService.getAllEntity());
		return "produzioni";
	}
	
	//ricerco un titolo che inizia con la stringa passata come parametro di input 
	@GetMapping("/filtroTitoloProd")
	public String filtroTitolo(@Param ("ricerca") String ricerca, @Param ("paramTitolo") String paramTitolo, Model model) {
		if(ricerca.contentEquals("inizio")) {
			model.addAttribute("listProduzioni", cinemaService.searchTitolo(paramTitolo));
		}
		else {
			model.addAttribute("listProduzioni", cinemaService.filteredTitolo(paramTitolo));
		}
		return "produzioni";
	}
}
