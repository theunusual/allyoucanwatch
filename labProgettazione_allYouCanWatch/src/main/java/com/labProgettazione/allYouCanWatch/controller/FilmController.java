package com.labProgettazione.allYouCanWatch.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.labProgettazione.allYouCanWatch.model.Attore;
import com.labProgettazione.allYouCanWatch.model.Film;
import com.labProgettazione.allYouCanWatch.service.EntityService;
import com.labProgettazione.allYouCanWatch.service.FilmServiceImpl;

@Controller
public class FilmController {
	
	@Autowired
	private FilmServiceImpl filmService;

	@Autowired
	private EntityService<Attore> attoreService;
	
	@GetMapping("/film")
	public String viewFilmPage(Model model){
		model.addAttribute("listFilm", filmService.getAllEntity());
		return "film";
	}
	
	@GetMapping("/newFilmForm")
	public String newFilmForm(Model model) {
		//creazione dell'attributo model per abbinare i dati del form
		Film film = new Film();
		model.addAttribute("film", film);
		model.addAttribute("listAttori", attoreService.getAllEntity());
		return "newFilm";
	}
	
	@PostMapping("/saveFilm")
	public String saveFilm(@ModelAttribute("film") Film film) {
		//salvo nel database
		filmService.saveEntity(film);
		return "redirect:/film";
	}
	
	@PostMapping("/saveUpdateFilm")
	public String saveUpdateFilm(@ModelAttribute("attore") Film film) {
		filmService.updateEntity(film);
		return "redirect:/film";
	}
		
	@GetMapping("updateFilm/{id}")
	public String updateFilm(@PathVariable (value = "id") Long id, Model model) {
		// get film dal service
		Film film = (Film) filmService.getEntityById(id);
		// set film come un attiributo model da un form pre-popolato
		model.addAttribute("film", film);
		model.addAttribute("listfilm", filmService.getAllEntity());
		model.addAttribute("listAttori", attoreService.getAllEntity());
		return "updateFilm";
	}

	@GetMapping("/deleteFilm/{id}")
	public String deleteFilm(@PathVariable (value = "id") Long id) {
		//richiamo il metodo di cancellazione della film
		this.filmService.deleteEntityById(id);
		return "redirect:/film";
	}
	
	//ricerco un titolo che inizia con la stringa passata come parametro di input 
	@GetMapping("/filtroTitoloFilm")
	public String filtroTitolo(@Param ("ricerca") String ricerca, @Param ("paramTitolo") String paramTitolo, Model model) {
		if(ricerca.contentEquals("inizio")) {
			model.addAttribute("listFilm", filmService.searchTitolo(paramTitolo));
		}
		else {
			model.addAttribute("listFilm", filmService.filteredTitolo(paramTitolo));
		}
		return "film";
	}
	
}
