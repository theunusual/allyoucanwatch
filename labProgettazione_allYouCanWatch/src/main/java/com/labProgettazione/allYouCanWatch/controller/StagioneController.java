package com.labProgettazione.allYouCanWatch.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.labProgettazione.allYouCanWatch.model.SerieTv;
import com.labProgettazione.allYouCanWatch.model.Stagione;
import com.labProgettazione.allYouCanWatch.service.EntityService;

@Controller
public class StagioneController {

	@Autowired
	private EntityService<Stagione> stagioneService;
	
	@Autowired
	private EntityService<SerieTv> serieTvService;
	
	//stampa delle stagioni presenti nel database
	@GetMapping("/stagioni/{id}")
	public String viewStagioniPageById(@PathVariable (value = "id") Long id, Model model) {
		//se si vuole vedere l'intera lista del DB basta implementare un if con id = 0 che indica tutte le stagioni
		model.addAttribute("serie", serieTvService.getEntityById(id));
		return "stagioni";
	}
	
	//stampa della trama della stagione
		@GetMapping("/trama_stagione/{id_stagione}")
		public String tramaStagione(@PathVariable (value = "id_stagione") Long id, Model model) {
			//se si vuole vedere l'intera lista del DB basta implementare un if con id = 0 che indica tutte le stagioni
			Stagione stagione = (Stagione) stagioneService.getEntityById(id);
			model.addAttribute("titolo", stagione.getSerieTv().getTitolo());
			model.addAttribute("trama", stagione.getTrama());
			return "trama";
		}
	
	@GetMapping("/newStagioneForm/{id}")
	public String newStagioneForm(@PathVariable (value = "id") Long id, Model model) {
		//creazione dell'attributo model per abbinare i dati del form
		Stagione stagione= new Stagione();
		model.addAttribute("serie", serieTvService.getEntityById(id));
		model.addAttribute("stagione", stagione);
		return "newStagione";
	}
	
	@PostMapping("/saveStagione")
	public String saveStagione(@ModelAttribute("stagione") Stagione stagione) {
		//salvo nel database
		stagioneService.saveEntity(stagione);
		return "redirect:/stagioni/" + stagione.getSerieTv().getId();
	}
	
	@GetMapping("updateStagione/{id}")
	public String updateStagione(@PathVariable (value = "id") Long id, Model model) {
		// get attore dal service
		Stagione stagione = (Stagione) stagioneService.getEntityById(id);
		// set attore come un attiributo model da un form pre-popolato
		model.addAttribute("stagione", stagione);
		model.addAttribute("serie", stagione.getSerieTv());
		return "updateStagione";
	}
	
	@GetMapping("/deleteStagione/{id}")
	public String deleteStagione(@PathVariable (value = "id") Long id) {
		//richiamo il metodo di cancellazione dell'attore
		Stagione stagione = (Stagione) stagioneService.getEntityById(id);
		this.stagioneService.deleteEntityById(id);
		return "redirect:/stagioni/" + stagione.getSerieTv().getId();
	}
}
