package com.labProgettazione.allYouCanWatch.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.labProgettazione.allYouCanWatch.model.Attore;
import com.labProgettazione.allYouCanWatch.model.SerieTv;
import com.labProgettazione.allYouCanWatch.service.EntityService;
import com.labProgettazione.allYouCanWatch.service.SerieTvServiceImpl;

@Controller
public class SerieTVController {

	@Autowired
	private SerieTvServiceImpl serieTvService;
	
	@Autowired
	private EntityService<Attore> attoreService;
	
	//stampa delle serie tv presenti nel database
	@GetMapping("/serieTv")
	public String viewSerieTvPage(Model model){
		model.addAttribute("listSerieTv", serieTvService.getAllEntity());
		return "serieTv";
	}
	
	@GetMapping("/newSerieTvForm")
	public String newSerieTvForm(Model model) {
		//creazione dell'attributo model per abbinare i dati del form
		SerieTv serieTv = new SerieTv();
		model.addAttribute("serieTv", serieTv);
		model.addAttribute("listSerieTv", serieTvService.getAllEntity());
		model.addAttribute("listAttori", attoreService.getAllEntity());
		return "newSerieTv";
	}
	
	@PostMapping("/saveSerieTv")
	public String saveSerieTv(@ModelAttribute("serieTv") SerieTv serieTv) {
		//salvo nel database
		serieTvService.saveEntity(serieTv);
		return "redirect:/serieTv";
	}
	
	
	@PostMapping("/saveUpdateSerieTv")
	public String saveUpdateSerieTv(@ModelAttribute("attore") SerieTv serie) {
		serieTvService.updateEntity(serie);
		return "redirect:/serieTv";
	}
		
	@GetMapping("updateSerieTv/{id}")
	public String updateSerieTv(@PathVariable (value = "id") Long id, Model model) {
		// get serieTv dal service
		SerieTv serieTv = (SerieTv) serieTvService.getEntityById(id);
		// set serieTv come un attiributo model da un form pre-popolato
		model.addAttribute("serieTv", serieTv);
		model.addAttribute("listSerieTv", serieTvService.getAllEntity());
		model.addAttribute("listAttori", attoreService.getAllEntity());
		return "updateSerieTv";
	}
	
	@GetMapping("/deleteSerieTv/{id}")
	public String deleteSerieTv(@PathVariable (value = "id") Long id) {
		//richiamo il metodo di cancellazione della serieTv
		this.serieTvService.deleteEntityById(id);
		return "redirect:/serieTv";
	}
	
	@GetMapping("/search")
	public String searchForm(Model model) {
		//creazione dell'attributo model per abbinare i dati del form
		SerieTv serieTv = new SerieTv();
		model.addAttribute("serieTv", serieTv);
		model.addAttribute("listSerieTv", serieTvService.getAllEntity());
		return "search";
	}
	
	@GetMapping("/searchSerieTv")
	public String searchSerieTvPage(@Param ("paramTitolo") String paramTitolo, Model model){
		List<SerieTv> listFiltered = serieTvService.filteredTitolo(paramTitolo);
		// set serieTv come un attiributo model da un form pre-popolato
		model.addAttribute("listFilteredserieTv", listFiltered);
		return "search";
	}
	
	//ricerco un titolo che inizia con la stringa passata come parametro di input 
	@GetMapping("/filtroTitoloSerie")
	public String filtroTitolo(@Param ("ricerca") String ricerca, @Param ("paramTitolo") String paramTitolo, Model model) {
		if(ricerca.contentEquals("inizio")) {
			model.addAttribute("listSerieTv", serieTvService.searchTitolo(paramTitolo));
		}
		else {
			model.addAttribute("listSerieTv", serieTvService.filteredTitolo(paramTitolo));
		}
		return "serieTv";
	}

}
