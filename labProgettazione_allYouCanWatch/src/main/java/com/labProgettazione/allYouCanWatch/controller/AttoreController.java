package com.labProgettazione.allYouCanWatch.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.labProgettazione.allYouCanWatch.model.Attore;
import com.labProgettazione.allYouCanWatch.service.EntityService;

@Controller
public class AttoreController {
	
	@Autowired
	private EntityService<Attore> attoreService;
	
	//stampa degli attori presenti nel database
	@GetMapping("/attori")
	public String viewAttoriPage(Model model){
		model.addAttribute("listAttori", attoreService.getAllEntity());
		return "attori";
	}
	
	
	@GetMapping("/newAttoreForm")
	public String newAttoreForm(Model model) {
		//creazione dell'attributo model per abbinare i dati del form
		Attore attore = new Attore();
		model.addAttribute("attore", attore);
		return "newAttore";
	}
	
	@PostMapping("/saveAttore")
	public String saveAttore(@ModelAttribute("attore") Attore attore) {
		//salvo nel database
		attoreService.saveEntity(attore);
		return "redirect:/attori";
	}
	
	@PostMapping("/saveUpdateAttore")
	public String saveUpdateAttore(@ModelAttribute("attore") Attore attore) {
		attoreService.updateEntity(attore);
		return "redirect:/attori";
	}
	
	@GetMapping("updateAttore/{id}")
	public String updateAttore(@PathVariable (value = "id") Long id, Model model) {
		// get attore dal service
		Attore attore = (Attore) attoreService.getEntityById(id);
		// set attore come un attiributo model da un form pre-popolato
		model.addAttribute("attore", attore);
		return "updateAttore";
	}
	
	@GetMapping("/deleteAttore/{id}")
	public String deleteAttore(@PathVariable (value = "id") Long id) {
		//richiamo il metodo di cancellazione dell'attore
		this.attoreService.deleteEntityById(id);
		return "redirect:/attori";
	}

}
