package com.labProgettazione.allYouCanWatch.controller;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.labProgettazione.allYouCanWatch.auxiliary.RegistrazioneUtenteDto;
import com.labProgettazione.allYouCanWatch.model.Utente;
import com.labProgettazione.allYouCanWatch.service.UtenteService;

@Controller
@RequestMapping("/registrazione")
public class RegistrazioneUtenteController {

    @Autowired
    private UtenteService utenteService;

    public RegistrazioneUtenteController(UtenteService utenteService) {
		super();
		this.utenteService = utenteService;
	}
    
    public RegistrazioneUtenteController() {
    	super();
    }
    
    // associazione attributo utente a oggetto di tipo RegistrazioneUtenteDto
    @ModelAttribute("user")
    public RegistrazioneUtenteDto registrazioneUtenteDto() {
        return new RegistrazioneUtenteDto();
    }
    
    //mapping a form di registrazione
    @GetMapping
    public String mostraFormRegistrazione(Model modello) {
    	modello.addAttribute("utente", new RegistrazioneUtenteDto());
        return "registrazione";
    }
    
    //controllo validazione mail
    public boolean isValidEmail(String email) {
        EmailValidator validator = EmailValidator.getInstance();
        return validator.isValid(email);
    }
    
    // controllo validazione password
    public String isValidPassword(String password){
         
    	//msg rimane vuoto se la password è valida
    	String msg = ""; 
       	if (password.length() < 8)
       			msg = "La password deve avere almeno 8 caratteri";
        String upperCaseChars = "(.*[A-Z].*)";
        if (!password.matches(upperCaseChars )) {
        	if(msg == "")
        		msg ="La password deve avere almeno un carattere maiuscolo";
        	else
        		msg = msg + ", un carattere maiuscolo";
        }
        String lowerCaseChars = "(.*[a-z].*)";
        if (!password.matches(lowerCaseChars )) {
        	if(msg == "")
        		msg = "La password deve avere almeno un carattere minuscolo";
        	else
        		msg = msg + ", un carattere minuscolo";
        }
        String numbers = "(.*[0-9].*)";
        if (!password.matches(numbers )) {
        	if(msg == "")
        		msg = "La password deve avere almeno un numero";
        	else
        		msg = msg + ", un numero";
        }
        String specialChars = "(.*[@,#,$,%,.].*$)";
        if (!password.matches(specialChars)) {
        	if (msg == "")
        		msg = "La password deve avere almeno un carattere speciale tra @ # . $ %";
        	else
        		msg = msg + ", un carattere speciale tra @ # . $ %";
        }
       if (msg != "" && msg.lastIndexOf(",") > - 1) {
    	   int pos = msg.lastIndexOf(",");
    	   msg = msg.substring(0, pos) + " e" + msg.substring(pos + 1, msg.length());
       }
        return msg;
    }
    
    // controllo matching tra due campi 
    public boolean matchingCampi(String campo1, String campo2) {
        return campo1!= null && campo2 != null && campo1.equals(campo2);
    }
    
    
    // post mapping per registrazione utente con validazione mail e password e matching tra password e conferma password
    @PostMapping
    public String RegistrazioneAccountUtente(@ModelAttribute("utente" )  RegistrazioneUtenteDto utenteDto, BindingResult risultato) {
    	boolean errore = false;
    	Utente esistente = utenteService.findByEmail(utenteDto.getEmail());
    	if(esistente!= null) {
    		errore = true;
    		
    		// messaggio errore stampato all'utente
    		risultato.rejectValue("email", null,"Questa email risulta associata a un account già esistente");
    	}
    	if(!isValidEmail(utenteDto.getEmail())) {
    		errore = true;
    		
    		// messaggio errore stampato all'utente
    		risultato.rejectValue("email", null,"Mail non valida");
    	}
    	String msg = isValidPassword(utenteDto.getPassword());
    	if(msg != "") {
    		
    		// messaggio errore stampato all'utente
    		risultato.rejectValue("password", null, msg);
    		errore = true;
    	}
    	
    	if(!matchingCampi(utenteDto.getPassword(), utenteDto.getConfermaPassword())) {
    		errore = true;
    		
    		// messaggio errore stampato all'utente
    		risultato.rejectValue("confermaPassword" ,null, "Le password non corrispondono");
    	}
    	
    	//ritorno a pagina di registrazione in caso di errore di validazione
    	if (risultato.hasErrors() || errore) {
            return "registrazione";
        }
    	
    	//richiamo service per salvataggio utente in caso di successo e restituzione messaggio di successo
    	utenteService.save(utenteDto);
		return "redirect:/registrazione?success";
    }
    
   
}