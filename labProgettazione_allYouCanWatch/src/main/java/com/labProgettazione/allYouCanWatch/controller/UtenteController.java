package com.labProgettazione.allYouCanWatch.controller;

import java.security.Principal;
import java.security.SecureRandom;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.labProgettazione.allYouCanWatch.auxiliary.MailMail;
import com.labProgettazione.allYouCanWatch.model.Utente;
import com.labProgettazione.allYouCanWatch.service.UtenteServiceImpl;
import org.springframework.beans.factory.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.xml.XmlBeanFactory; 
import org.springframework.core.io.*;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;  

@SuppressWarnings("deprecation")
@Controller
public class UtenteController {
	
	@Autowired
	UtenteServiceImpl utenteServiceImpl;
	
	@Autowired
	RegistrazioneUtenteController registrazioneUtenteController;
    
	//mapping a pagina di login
    @GetMapping("/login")
    public String login(Model model) {
        return "login";
    }
    
    //mapping a pagina password smarrita
    @GetMapping("/passwordSmarrita")
    public String passwordSmarrita(Model model) {
        return "passwordSmarrita";
    }
    
    /*
     * mapping a pagina personale con estrazione informazioni utente loggato tramite
     * chiamata a metodo loadUserInfoByUsername dell'oggetto utenteServiceImpl
     */
    
    @GetMapping("/paginaPersonale")
    public String paginaPersonale(Model model, Principal principal) {
    	if(principal != null) {
			String email = principal.getName();
			Utente u = utenteServiceImpl.loadUserInfoByUsername(email);
			model.addAttribute("u", u);
		}
        return "paginaPersonale";
    }
    
    // mapping a pagina di modifica password
    @GetMapping("/modificaPassword")
    public String modificaPassword() {
        return "modificaPassword";
    }
    
    /* 
     * request mapping con passaggio parametri per modificare la password
     * previsti controlli relativi alla nuova password (validazione) e matching
     * con il conferma nuova password
    */
    @RequestMapping(value = "/inviaPasswordModifica", method = RequestMethod.POST)
   	public String modificaPassword(@RequestParam("password") String newPassword, @RequestParam("confermaPassword") String confirmPassword, @RequestParam("vecchiaPassword") String oldPassword, Principal principal) {
    	String email = principal.getName();
    	UserDetails u = utenteServiceImpl.loadUserByUsername(email);
    	PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    	String msg = registrazioneUtenteController.isValidPassword(newPassword);
    	String redirect = "";
    	if(msg != "") {
    		redirect = redirect + "pswNotValid";
    	}
    	if(!registrazioneUtenteController.matchingCampi(newPassword, confirmPassword)) {
    		if (redirect != "")
    			redirect = redirect + "&";
    		redirect = redirect + "notMatching";
    	}
    	if(!passwordEncoder.matches(oldPassword,u.getPassword())) {
    		if (redirect != "")
    			redirect = redirect + "&";
    		redirect = redirect + "wrongOld";
    	}
    	
    	// indirizzamento a pagina personale con messaggio di successo in caso di validazione e matching ok
    	if (redirect == "") {
    		utenteServiceImpl.updateUser(email, newPassword);
    		redirect = "redirect:paginaPersonale?pswSuccess";
    	}
    	// indirizzamento a pagina modificapassword con messaggio di errore in caso di validazione e matching falliti
    	else
    		redirect = "redirect:modificaPassword?" + redirect;
    	
   		return redirect;
    }
    
    //generatore password randomica e temporanea
    public static String generatorePass(int len)
    {
        // ASCII range – alfanumerico (0-9, a-z, A-Z)
        final String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
 
        SecureRandom random = new SecureRandom();
        StringBuilder sb = new StringBuilder();
 
        // ad ogni iterazione randomicamente sceglie un carattere
        for (int i = 0; i < len; i++)
        {
            int indiceRandom = random.nextInt(chars.length());
            sb.append(chars.charAt(indiceRandom));
        }
 
        return sb.toString();
    }
 
    /*
     * request mapping con mail passata come parametro per caricare utente tramite
     * metodo di utenteServiceImpl, generazione password casuale e temporanea,
     * invio mail tramite metodo sendMail della classe MailMail, il cui testo
     * prevede la password generata
     * modifica password utente con password casuale
     */
    @RequestMapping(value = "/inviaEmail", method = RequestMethod.POST)
	public String inviaEmail(@RequestParam("username") String email) {
        try {
	    	utenteServiceImpl.loadUserByUsername(email);
	        String password = generatorePass(10);
	        
	        // richiamo bean di configurazione proprietà email
	    	Resource r=new ClassPathResource("applicationContext.xml");  
			BeanFactory b=new XmlBeanFactory(r);  
	    	MailMail m=(MailMail)b.getBean("mailMail");  
	    	
	    	//settaggio mittente e destinatario
	    	String sender="milbospas@gmail.com";
	    	String receiver=email;
	    	
	    	//aggiornamento password utente
	    	utenteServiceImpl.updateUser(email, password);
	    	
	    	//invio mail
	    	m.sendMail(sender,receiver,"All You Can Watch - Ripristino password", 
	    			"Gentile utente,\n\nQuesta è la sua nuova password temporanea: "+password+
	    			"\n\nLa preghiamo di modificarla al più presto nella sezione opportuna del nostro sito."
	    			+ "\n\nCordialmente,\n\nAll You Can Watch");  
	    	
	    	// redirect a login in caso di mail inviata con successo
	    	return "redirect:/login?ripristino";
	    	
        }catch(Exception e) {
        	
        	//generazione eccezione in caso di fallimento invio mail
    		return "redirect:/passwordSmarrita?error";
    	}
    }
    

    
   
	
}