package com.labProgettazione.allYouCanWatch.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.labProgettazione.allYouCanWatch.model.Utente;
import com.labProgettazione.allYouCanWatch.service.UtenteServiceImpl;


@Controller
public class HomeController {
	
	@Autowired
	UtenteServiceImpl utenteServiceImpl;
	
		/* 
		 * mapping pagina iniziale con eventuale (in caso di autenticazione) 
		 * caricamento nome account loggato per messaggio di benvenuto
		 */
		@GetMapping("/")
		public String viewIndexPage(Model model, Principal principal){
			if(principal != null) {
				String email = principal.getName();
				Utente u = utenteServiceImpl.loadUserInfoByUsername(email);
				model.addAttribute("u", u);
			}
			return "index";
		}
		
		
		 @GetMapping("/index")
		    public String root() {
		        return "index";
		    }
		 
		 

}
