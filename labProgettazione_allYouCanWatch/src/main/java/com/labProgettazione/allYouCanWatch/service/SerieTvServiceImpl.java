package com.labProgettazione.allYouCanWatch.service;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.labProgettazione.allYouCanWatch.model.SerieTv;
import com.labProgettazione.allYouCanWatch.repository.SerieTvRepository;

@Service
public class SerieTvServiceImpl implements EntityService<SerieTv>{

	@Autowired
	private SerieTvRepository serieTvRepository;
	
	@Override
	public List<SerieTv> getAllEntity() {
		return serieTvRepository.findAll(Sort.by(Sort.Direction.ASC, "titolo"));
	}
	
	@Override
	public List<SerieTv> getAllEntitySortedById() {
		return serieTvRepository.findAll();
	}
	
	@Override
	public void saveEntity(SerieTv serieTv) {
		this.serieTvRepository.save(serieTv);
	}

	@Override
	public SerieTv getEntityById(Long id) {
		Optional<SerieTv> optional = serieTvRepository.findById(id);
		SerieTv serieTv = null;
		if(optional.isPresent()) {
			serieTv = optional.get();
		}else {
			throw new RuntimeException("Serie tv non trovata per id :: " + id);
		}
		return serieTv;
	}
	
	@Override
	public void deleteEntityById(Long id) {
		this.serieTvRepository.deleteById(id);
	}
	
	@Override
	public List <SerieTv> filteredTitolo(String titolo) {
		List <SerieTv> a = serieTvRepository.findFiltered(titolo);
		return a;
	}
	
	@Override
	public List<SerieTv> searchTitolo(String titolo) {
		List <SerieTv> a = serieTvRepository.filtroTitolo(titolo);
		return a;
	}

	@Override
	public void updateEntity(SerieTv serie) {
		SerieTv newSerie = this.serieTvRepository.findById(serie.getId()).get();
		newSerie.setGenere(serie.getGenere());
		newSerie.setIn_corso(serie.getIn_corso());
		newSerie.setLingua_originale(serie.getLingua_originale());
		newSerie.setScenegg_originale(serie.getScenegg_originale());
		newSerie.setSerieTvSpinOff(serie.getSerieTvSpinOff());
		newSerie.setTitolo(serie.getTitolo());
		newSerie.setTitolo_sigla(serie.getTitolo_sigla());
		newSerie.setTrama(serie.getTrama());
		newSerie.setAttori(serie.getAttori());
		newSerie.setTipologia_produzione(serie.getTipologia_produzione());
		this.serieTvRepository.save(newSerie);
	}
	
}
