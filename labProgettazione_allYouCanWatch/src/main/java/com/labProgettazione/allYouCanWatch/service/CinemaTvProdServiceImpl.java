package com.labProgettazione.allYouCanWatch.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.labProgettazione.allYouCanWatch.model.CinemaTvProd;
import com.labProgettazione.allYouCanWatch.repository.CinemaTvProdRepository;

@Service
public class CinemaTvProdServiceImpl implements EntityService<CinemaTvProd>{
	
	@Autowired
	private CinemaTvProdRepository cinemaRepository;

	@Override
	public List<CinemaTvProd> getAllEntity() {
		return this.cinemaRepository.findAll(Sort.by(Sort.Direction.ASC, "titolo"));
	}

	@Override
	public List<?> getAllEntitySortedById() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void saveEntity(CinemaTvProd entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object getEntityById(Long id) {
		Optional<CinemaTvProd> optional = cinemaRepository.findById(id);
		CinemaTvProd cinema = null;
		if(optional.isPresent()) {
			cinema = optional.get();
		}else {
			throw new RuntimeException("Produzione non trovata per id :: " + id);
		}
		return cinema;
	}

	@Override
	public void deleteEntityById(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateEntity(CinemaTvProd entity) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	//trova titolo cercando la stringa passaata come parametro di input all'interno
	public List <CinemaTvProd> filteredTitolo(String titolo) {
		List <CinemaTvProd> a = cinemaRepository.findFiltered(titolo);
		return a;
	}
	
	@Override
	//trova titolo cercando la stringa passaata come parametro di input dall'inizio
	public List<CinemaTvProd> searchTitolo(String titolo) {
		List <CinemaTvProd> a = cinemaRepository.filtroTitolo(titolo);
		return a;
	}
	

}
