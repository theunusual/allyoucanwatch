package com.labProgettazione.allYouCanWatch.service;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.labProgettazione.allYouCanWatch.model.Ruolo;
import com.labProgettazione.allYouCanWatch.repository.RuoloRepository;

// service entità ruolo
@Service
public class RuoloServiceImpl implements EntityService<Ruolo>{
	
	//attributi service
	@Autowired
	private RuoloRepository ruoloRepository;
	
	//metodi service
	@Override
	public List<?> getAllEntity() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<?> getAllEntitySortedById() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void saveEntity(Ruolo entity) {
		// TODO Auto-generated method stub
		this.ruoloRepository.save(entity);
	}
	
	// restituisce il ruolo avente come id il valore passato come parametro
	@Override
	public Object getEntityById(Long id) {
		// TODO Auto-generated method stub
		Optional<Ruolo> optional = ruoloRepository.findById(id);
		Ruolo ruolo = null;
		if(optional.isPresent()) {
			ruolo = optional.get();
		}else {
			throw new RuntimeException("Ruolo non trovato per id :: " + id);
		}
		return ruolo;
	}

	@Override
	public void deleteEntityById(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateEntity(Ruolo entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<?> filteredTitolo(String paramTitolo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<?> searchTitolo(String paramTitolo) {
		// TODO Auto-generated method stub
		return null;
	}

}
