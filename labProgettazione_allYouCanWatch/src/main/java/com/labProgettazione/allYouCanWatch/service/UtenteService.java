package com.labProgettazione.allYouCanWatch.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.labProgettazione.allYouCanWatch.auxiliary.RegistrazioneUtenteDto;
import com.labProgettazione.allYouCanWatch.model.Utente;

// interfaccia service per entità utente
public interface UtenteService extends UserDetailsService{

	//dichiarazione metodi 
    Utente save(RegistrazioneUtenteDto registration);
    
    Utente findByEmail(String email);
    
    Utente loadUserInfoByUsername(String username);
    
    void updateUser(String username, String password);
    
    void delete(Long id);
    
    
}