package com.labProgettazione.allYouCanWatch.service;

import java.util.List;

public interface EntityService <T>{
	//restituisce una lista ordinata secondo un criterio di tutti gli elementi di tipo "Entity" (una delle classi nel package model)
	List<?> getAllEntity();
	
	//restituisce una lista ordinata per Id (ordine di tabella)
	List<?> getAllEntitySortedById();
	
	//salva permanentemente un entità passata come paramentro di input
	void saveEntity(T entity);
	
	//restituisce un entità dando in input il suo id
	Object getEntityById(Long id);
	
	//cancella un entità dando in input il suo id
	void deleteEntityById(Long id);
	
	//aggiorna l'entità passata come parametro di input
	void updateEntity(T entity);
	
	//restituisce le entità il cui titolo contiene al suo interno la stringa passata come parametro di input
	public List<?> filteredTitolo(String paramTitolo);
	
	//restituisce le entità il cui titolo inizia con la stringa passata come parametro di input
	public List<?> searchTitolo(String paramTitolo);
}
