package com.labProgettazione.allYouCanWatch.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.labProgettazione.allYouCanWatch.model.Film;
import com.labProgettazione.allYouCanWatch.repository.FilmRepository;

@Service
public class FilmServiceImpl implements EntityService<Film>{
	
	@Autowired 
	private FilmRepository filmRepository;
	
	@Override
	public List<Film> getAllEntity() {
		return this.filmRepository.findAll(Sort.by(Sort.Direction.ASC, "titolo"));
	}
	
	@Override
	public List<Film> getAllEntitySortedById() {
		return this.filmRepository.findAll();
	}

	@Override
	public void saveEntity(Film film) {
		this.filmRepository.save(film);	
	}

	@Override
	public Film getEntityById(Long id) {
		Optional<Film> optional = filmRepository.findById(id);
		Film film = null;
		if(optional.isPresent()) {
			film = optional.get();
		}else {
			throw new RuntimeException("Film non trovata per id :: " + id);
		}
		return film;
	}

	@Override
	public void deleteEntityById(Long id) {
		this.filmRepository.deleteById(id);	
	}

	@Override
	public void updateEntity(Film film) {
		Film newFilm = this.filmRepository.findById(film.getId()).get();
		newFilm.setTipo(film.getTipo());
		newFilm.setAnno_uscita(film.getAnno_uscita());
		newFilm.setDurata_min(film.getDurata_min());
		newFilm.setTitolo(film.getTitolo());
		newFilm.setTrama(film.getTrama());
		newFilm.setScenegg_originale(film.getScenegg_originale());
		newFilm.setLingua_originale(film.getLingua_originale());
		newFilm.setGenere(film.getGenere());
		newFilm.setAttori(film.getAttori());
		newFilm.setTipologia_produzione(film.getTipologia_produzione());
		this.filmRepository.save(newFilm);
	}
	
	@Override
	public List <Film> filteredTitolo(String titolo) {
		List <Film> a = filmRepository.findFiltered(titolo);
		return a;
	}
	
	@Override
	public List<Film> searchTitolo(String titolo) {
		List <Film> a = filmRepository.filtroTitolo(titolo);
		return a;
	}
	

}
