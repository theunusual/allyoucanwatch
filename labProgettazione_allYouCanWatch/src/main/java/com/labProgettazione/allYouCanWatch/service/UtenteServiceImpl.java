package com.labProgettazione.allYouCanWatch.service;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.labProgettazione.allYouCanWatch.auxiliary.RegistrazioneUtenteDto;
import com.labProgettazione.allYouCanWatch.model.Ruolo;
import com.labProgettazione.allYouCanWatch.model.Utente;
import com.labProgettazione.allYouCanWatch.repository.UtenteRepository;

// service entità utente
@Service
public class UtenteServiceImpl implements UtenteService {

	//attributi service
    @Autowired
    private UtenteRepository utenteRepository;
    
    @Autowired
    private RuoloServiceImpl ruoloService;
    
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;
    
    // costruttore service
    public UtenteServiceImpl(UtenteRepository utenteRepository) {
		super();
		this.utenteRepository = utenteRepository;
	}
    
    // metodi service
    
    // restituisce il valore di ritorno del metodo del repository dell'utente (ricerca utente da email)
    public Utente findByEmail(String email) {
        return utenteRepository.findByEmail(email);
    }
    
    // restituisce il valore di ritorno del metodo save del repository dell'utente (salvataggio utente)
    public Utente save(RegistrazioneUtenteDto registrazioneDto) {
    	
    	//creazione oggetto ruolo con id = 1 (id che lo identifica come ruolo Utente Premium)
    	Ruolo ruolo = (Ruolo) ruoloService.getEntityById(1L);
    	
        Utente utente = new Utente(registrazioneDto.getNome(), registrazioneDto.getCognome(),
        		registrazioneDto.getEmail(),
        		passwordEncoder.encode(registrazioneDto.getPassword()), Arrays.asList(ruolo));
    	Collection<Ruolo> ruoli=utente.getRuoli();
    	Iterator<Ruolo> iter = ruoli.iterator();
        System.out.println(iter.next());

        return utenteRepository.save(utente);
    }
    
    //eliminazione utente da id (effettuato con chiamata metodo a repository utente)
    public void delete(Long id) {
    	this.utenteRepository.deleteById(id);
	}
    
    // caricamento oggetto di tipo UserDetails da email 
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Utente utente = utenteRepository.findByEmail(username);
        if (utente == null) {
            throw new UsernameNotFoundException("Username o password non validi.");
        }
        return new org.springframework.security.core.userdetails.User(utente.getEmail(),
            utente.getPassword(),
            mapRolesToAuthorities(utente.getRuoli()));
    }
    
    // caricamento utente da email
    @Override
    public Utente loadUserInfoByUsername(String username)throws UsernameNotFoundException{
        Utente utente = utenteRepository.findByEmail(username);
        if (utente == null) {
            throw new UsernameNotFoundException("Username o password non validi.");
        }
    	return utente;
    }
    
    // aggiornamento password utente (con salvataggio nuove informazioni tramite chiamata metodo repository)
    @Override
	public void updateUser(String username, String password) {
    	Utente utente = utenteRepository.findByEmail(username);
    	Collection<Ruolo> ruoli = utente.getRuoli();
    	Iterator<Ruolo> it = ruoli.iterator();
    	Ruolo r = (Ruolo) it.next();
    	System.out.println(r.getNome());
    	if (r.getNome().equals("Utente Premium")) {
    		password = BCrypt.hashpw(password, BCrypt.gensalt(12));
        	utente.setPassword(password);
    		this.utenteRepository.save(utente);
    	} else {
    		 throw new UsernameNotFoundException("Username o password non validi.");
    	}
	}
    
    // mapping ruoli per oggetto di tipo Principal (Spring)
	private Collection < ? extends GrantedAuthority > mapRolesToAuthorities(Collection < Ruolo > ruoli) {
        return ruoli.stream()
            .map(ruolo -> new SimpleGrantedAuthority(ruolo.getNome()))
            .collect(Collectors.toList());
    }

    
}