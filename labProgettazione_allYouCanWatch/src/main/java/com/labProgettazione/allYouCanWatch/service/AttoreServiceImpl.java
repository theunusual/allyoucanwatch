package com.labProgettazione.allYouCanWatch.service;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.labProgettazione.allYouCanWatch.model.Attore;
import com.labProgettazione.allYouCanWatch.repository.AttoreRepository;

@Service
public class AttoreServiceImpl implements EntityService<Attore>{

	@Autowired
	private AttoreRepository attoreRepository;
	
	@Override
	public List<Attore> getAllEntity() {
		return attoreRepository.findAll(Sort.by(Sort.Direction.ASC, "nome").and(Sort.by(Sort.Direction.ASC, "cognome")));
	}
	
	@Override
	public List<Attore> getAllEntitySortedById() {
		return attoreRepository.findAll();
	}
	
	@Override
	public void saveEntity(Attore attore) {
		this.attoreRepository.save(attore);
	}

	@Override
	public Attore getEntityById(Long id) {
		Optional<Attore> optional = attoreRepository.findById(id);
		Attore attore = null;
		if(optional.isPresent()) {
			attore = optional.get();
		}else {
			throw new RuntimeException("Attore non trovato per id :: " + id);
		}
		return attore;
	}
	
	@Override
	public void deleteEntityById(Long id) {
		this.attoreRepository.deleteById(id);
	}
	
	@Override
	public void updateEntity(Attore attore) {
		Attore newAttore = this.attoreRepository.findById(attore.getId()).get();
		newAttore.setCinemaTvProd(attore.getCinemaTvProd());
		newAttore.setCognome(attore.getCognome());
		newAttore.setData_nascita(attore.getData_nascita());
		newAttore.setNome(attore.getNome());
		newAttore.setSesso(attore.getSesso());
		newAttore.setStato_nascita(attore.getStato_nascita());
		this.attoreRepository.save(newAttore);
	}

	@Override
	public List<?> filteredTitolo(String paramTitolo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<?> searchTitolo(String paramTitolo) {
		// TODO Auto-generated method stub
		return null;
	}

	
	
}
