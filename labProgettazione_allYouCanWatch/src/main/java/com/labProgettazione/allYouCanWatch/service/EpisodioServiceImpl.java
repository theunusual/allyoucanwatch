package com.labProgettazione.allYouCanWatch.service;
import org.springframework.data.domain.Sort;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.labProgettazione.allYouCanWatch.model.Episodio;
import com.labProgettazione.allYouCanWatch.repository.EpisodioRepository;

@Service
public class EpisodioServiceImpl implements EntityService<Episodio>{

	@Autowired
	private EpisodioRepository episodioRepository;
	
	@Override
	public List<Episodio> getAllEntity() {
		return episodioRepository.findAll(Sort.by(Sort.Direction.ASC, "stagione.serieTv.titolo").and(Sort.by(Sort.Direction.ASC, "stagione.numero").and(Sort.by(Sort.Direction.ASC, "numero"))));
	}
	
	@Override
	public void saveEntity(Episodio episodio) {
		this.episodioRepository.save(episodio);
	}

	@Override
	public Episodio getEntityById(Long id) {
		Optional<Episodio> optional = episodioRepository.findById(id);
		Episodio episodio = null;
		if(optional.isPresent()) {
			episodio = optional.get();
		}else {
			throw new RuntimeException("Episodio non trovato per id :: " + id);
		}
		return episodio;
	}
	
	@Override
	public void deleteEntityById(Long id) {
		this.episodioRepository.deleteById(id);
	}

	@Override
	public void updateEntity(Episodio episodio) {
		this.saveEntity(episodio);
		
	}

	@Override
	public List<?> getAllEntitySortedById() {
		return episodioRepository.findAll();
	}

	@Override
	public List<?> filteredTitolo(String paramTitolo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<?> searchTitolo(String paramTitolo) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
