package com.labProgettazione.allYouCanWatch.service;
import org.springframework.data.domain.Sort;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.labProgettazione.allYouCanWatch.model.Stagione;
import com.labProgettazione.allYouCanWatch.repository.StagioneRepository;

@Service
public class StagioneServiceImpl implements EntityService<Stagione>{

	@Autowired
	private StagioneRepository stagioneRepository;
	
	@Override
	public List<Stagione> getAllEntity() {
		return stagioneRepository.findAll(Sort.by(Sort.Direction.ASC, "serieTv.titolo").and(Sort.by(Sort.Direction.ASC, "numero")));
	}
	
	@Override
	public List<Stagione> getAllEntitySortedById() {
		return stagioneRepository.findAll();
	}
	
	@Override
	public void saveEntity(Stagione stagione) {
		this.stagioneRepository.save(stagione);
	}

	@Override
	public Stagione getEntityById(Long id) {
		Optional<Stagione> optional = stagioneRepository.findById(id);
		Stagione stagione = null;
		if(optional.isPresent()) {
			stagione = optional.get();
		}else {
			throw new RuntimeException("Stagione non trovata per id :: " + id);
		}
		return stagione;
	}
	
	@Override
	public void deleteEntityById(Long id) {
		this.stagioneRepository.deleteById(id);
	}

	@Override
	public void updateEntity(Stagione stagione) {
		this.saveEntity(stagione);
		
	}

	@Override
	public List<?> filteredTitolo(String paramTitolo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<?> searchTitolo(String paramTitolo) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
