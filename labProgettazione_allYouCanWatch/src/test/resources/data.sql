INSERT INTO attore (id, nome, cognome, data_nascita, sesso, stato_nascita) VALUES (NULL, 'Raffaello', 'Passarelli', '1992-02-02', 'M', 'Italia')
INSERT INTO attore (id, nome, cognome, data_nascita, sesso, stato_nascita) VALUES (NULL, 'Francesco', 'Passarelli', '1991-01-01', 'M', 'Italia')
INSERT INTO attore (id, nome, cognome, data_nascita, sesso, stato_nascita) VALUES (NULL, 'Will', 'Smith', '1986-03-03', 'M', 'Stati Uniti')


INSERT INTO episodio(id, ascolti, durata_min, numero, titolo, trama, stagione_id) VALUES (NULL, 300, 50, 1, 'episodio 1', 'trama 1', NULL)
INSERT INTO episodio(id, ascolti, durata_min, numero, titolo, trama, stagione_id) VALUES (NULL, 300, 50, 1, 'episodio 2', 'trama 2', NULL)
INSERT INTO episodio(id, ascolti, durata_min, numero, titolo, trama, stagione_id) VALUES (NULL, 300, 50, 1, 'episodio 2', 'trama 3', NULL)

INSERT INTO utente(id, cognome, email, nome, password) VALUES (10, 'cognomeTest', 'mail@test.it', 'nomeTest', '$2y$12$he4YFIjDD.w6OHI85r03/OcJ1Oe.UVxEIpCP2kA/./Hf6t/XO0p6. ')
INSERT INTO ruolo(id, nome) VALUES (0, 'Amministratore')
INSERT INTO utenti_ruoli(id_utente, id_ruolo) VALUES (10, 0)