package com.labProgettazione.allYouCanWatch;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.*;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.util.List;
import java.security.Principal;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringRunner;

import com.labProgettazione.allYouCanWatch.auxiliary.RegistrazioneUtenteDto;
import com.labProgettazione.allYouCanWatch.controller.RegistrazioneUtenteController;
import com.labProgettazione.allYouCanWatch.controller.UtenteController;
import com.labProgettazione.allYouCanWatch.model.Attore;
import com.labProgettazione.allYouCanWatch.model.CinemaTvProd;
import com.labProgettazione.allYouCanWatch.model.Episodio;
import com.labProgettazione.allYouCanWatch.model.Film;
import com.labProgettazione.allYouCanWatch.model.Ruolo;
import com.labProgettazione.allYouCanWatch.model.SerieTv;
import com.labProgettazione.allYouCanWatch.model.Stagione;
import com.labProgettazione.allYouCanWatch.model.Utente;
import com.labProgettazione.allYouCanWatch.service.EntityService;
import com.labProgettazione.allYouCanWatch.service.UtenteService;

import org.springframework.validation.BindingResult;


@RunWith(SpringRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@SpringBootTest(classes = LabProgettazioneAllYouCanWatch.class)
public class SpringBootJPATest {
	
	@Autowired
	private EntityService<Attore> attoreService;
	
	@Autowired
	private EntityService<SerieTv> serieTvService;
	
	@Autowired
	private EntityService<Episodio> episodioService;
	
	@Autowired
	private EntityService<Stagione> stagioneService;
	
	@Autowired
	private EntityService<Film> filmService;
	
	@Autowired
	private UtenteService utenteService;
	
	@Autowired
	private EntityService<Ruolo> ruoloService;
	
	@Autowired 
	private RegistrazioneUtenteController registrazioneUtenteController;

	@Autowired 
	private EntityService<CinemaTvProd> cinemaService;
	
	@Autowired UtenteController utenteController;
	
	
	@Test
	public void A_testCinemaTvProd() {
		//se la lista dei film è vuota inserisco un film poi elimino il primo film della lista e controllo
		//che nella lista il numero di film sia pari alla dimensione prima della cancellazione -1.
		List<CinemaTvProd> allCinemaList =  (List<CinemaTvProd>) cinemaService.getAllEntity();
		int num_cinema = allCinemaList.size();
		Film film = new Film();
		film.setTitolo("Titolo111");
		filmService.saveEntity(film);
		allCinemaList = (List<CinemaTvProd>) cinemaService.getAllEntity();
		assertEquals(num_cinema+1, allCinemaList.size());
		filmService.deleteEntityById(allCinemaList.get(0).getId());
	}



	@Test
	public void B_testDeleteFilm() {
		//se la lista dei film è vuota inserisco un film poi elimino il primo film della lista e controllo
		//che nella lista il numero di film sia pari alla dimensione prima della cancellazione -1.
		List<Film> allFilmList = (List<Film>) filmService.getAllEntitySortedById();
		if(allFilmList.size() == 0) {
			Film film = new Film();
			film.setTitolo("Titolo111");
			filmService.saveEntity(film);
			allFilmList = (List<Film>) filmService.getAllEntitySortedById();
		}
		assertNotNull(allFilmList);
		int num_film = allFilmList.size();
		filmService.deleteEntityById(allFilmList.get(0).getId());
		allFilmList = (List<Film>) filmService.getAllEntitySortedById();
		assertEquals(num_film-1, allFilmList.size());
	}



	//test di visualizzazione(find all)
	@Test
	public void C_testFindFilm() {
		Film film = new Film();
		film.setTitolo("Titolo111");
		filmService.saveEntity(film);
		Film film2 = new Film();
		film2.setTitolo("Titolo222");
		filmService.saveEntity(film2);
		
		List<Film> allFilmList = (List<Film>) filmService.getAllEntitySortedById();
		assertNotNull(allFilmList);
		assertEquals(2, allFilmList.size());
	}



	@Test
	public void D_testUpdateFilm() {
		Film film = new Film();
		film.setTitolo("Titolo111");
		filmService.saveEntity(film);
		
		List<Film> allFilmList = (List<Film>) filmService.getAllEntitySortedById();
		Film film2 = (Film) filmService.getEntityById(allFilmList.get(allFilmList.size()-1).getId());
		film2.setTitolo("Titolo222");
		filmService.updateEntity(film2);
		System.out.println(film2.getId());
		
		Film film3 = (Film) filmService.getEntityById(allFilmList.get(allFilmList.size()-1).getId());
		assertNotNull(film3);
		assertEquals(film.getId(), film3.getId());
		assertNotEquals(film.getTitolo(), film3.getTitolo());
		assertEquals(film3.getTitolo(), film2.getTitolo());
		
		filmService.deleteEntityById(allFilmList.get(0).getId());
	}



	@Test
	public void E_testFiltrotitolo() {
		//ricerca filtro produzioni cinematografiche
		Film film = new Film();
		film.setTitolo("Film111");
		filmService.saveEntity(film);
		Film film2 = new Film();
		film2.setTitolo("Film222");
		filmService.saveEntity(film2);
		List<CinemaTvProd> allCinemaList = (List<CinemaTvProd>) cinemaService.filteredTitolo("2");
		assertNotNull(allCinemaList);
		assertEquals(3, allCinemaList.size());
		allCinemaList = (List<CinemaTvProd>) cinemaService.searchTitolo("f");
		assertNotNull(allCinemaList);
		assertEquals(2, allCinemaList.size());
		
		//ricerca filtro film
		List<Film> allFilmList = (List<Film>) filmService.getAllEntitySortedById();
		allFilmList = (List<Film>) filmService.filteredTitolo("2");
		assertNotNull(allFilmList);
		assertEquals(3, allFilmList.size());
		allFilmList = (List<Film>) filmService.searchTitolo("f");
		assertNotNull(allFilmList);
		assertEquals(2, allFilmList.size());
		
		//ricarca filtro serie tv: salvo due serie con titoli diversi e le filtro
		SerieTv serie = new SerieTv();
		serie.setTitolo("Titolo serie 1");
		serie.setGenere("Commedia");
		serie.setIn_corso(false);
		serieTvService.saveEntity(serie);
		SerieTv serie2 = new SerieTv();
		serie2.setTitolo("Serie 2");
		serieTvService.saveEntity(serie2);
		List<SerieTv> allSerieList = (List<SerieTv>) serieTvService.filteredTitolo("s");
		assertNotNull(allSerieList);
		assertEquals(2, allSerieList.size());
		allSerieList = (List<SerieTv>) serieTvService.searchTitolo("s");
		assertNotNull(allSerieList);
		assertEquals(1, allSerieList.size());
	}



	// Test inserimento film
	@Test
	public void F_testSaveFilm() {
		
		List<Film> allFilmList = (List<Film>) filmService.getAllEntitySortedById();
		Film film = new Film();
		film.setTitolo("Titolo111");
		filmService.saveEntity(film);
		System.out.println("eccomi");
		allFilmList = (List<Film>) filmService.getAllEntitySortedById();
		Film trovaFilm = (Film) filmService.getEntityById(allFilmList.get(allFilmList.size()-1).getId());
		assertNotNull(trovaFilm);
		assertEquals(film.getTitolo(), trovaFilm.getTitolo());
		
		filmService.deleteEntityById(film.getId());
	}



	@Test
	public void G_testSerieStagione() {
		//creo una stagione e la salvo
		Stagione stagione1 = new Stagione();
		stagione1.setNumero(1);
		stagione1.setAnno_uscita(2020);
		stagione1.setTrama("trama stagione 1");
		stagioneService.saveEntity(stagione1);
		
		//creo una serieTV e la salvo
		SerieTv serie = new SerieTv();
		serie.setTitolo("How I met your mother");
		serie.setGenere("Commedia");
		serie.setIn_corso(false);
		serieTvService.saveEntity(serie);
		
		//ricerco la serie tv per verificare che sia stata salvata e testo la getEntityById (testo con assert)
		List<SerieTv> listaSerie = (List<SerieTv>) serieTvService.getAllEntitySortedById();
		SerieTv foundSerie = (SerieTv) serieTvService.getEntityById(listaSerie.get(listaSerie.size()-1).getId());
		Long id_serie = foundSerie.getId();
		//per esecuzione di test singolo usare la get qui sotto
		//SerieTv foundSerie = (SerieTv) serieTvService.getEntityById(1L);
		assertNotNull(foundSerie);
		assertEquals(foundSerie.getTitolo(), serie.getTitolo());
		
		//ricerco la stagione inserita per verificare che sia stata salvata e testo la getEntityById (testo con assert)
		Stagione foundStagione = (Stagione) stagioneService.getEntityById(1L);
		assertNotNull(foundStagione);
		assertEquals(foundStagione.getAnno_uscita(), 2020);
		
		//associo alla stagione la serie tv  e salvo (relazione ManyToOne)
		foundStagione.setSerieTv(foundSerie);
		stagioneService.saveEntity(foundStagione);
		// scarico dal db la lista delle stagioni e verifico che il titolo della serieTV della prima stagione sia quello atteso 
		List<Stagione> listStagioni = (List<Stagione>) stagioneService.getAllEntitySortedById();
		assertNotNull(listStagioni);
		assertEquals(listStagioni.get(0).getSerieTv().getTitolo(), serie.getTitolo());
		
		//relazione serieTv con stagioni, scarido dal db la serie tv e verifico che il numero delle stagioni sia quello atteso 
		foundSerie = (SerieTv) serieTvService.getEntityById(id_serie);
		//per esecuzione di test singolo usare la get qui sotto
		//SerieTv foundSerie = (SerieTv) serieTvService.getEntityById(1L);
		assertNotNull(foundSerie);
		assertEquals(foundSerie.getTitolo(), serie.getTitolo());
	}



	// test su serie tv: inserimento, aggiornamento, inserimento cast (relazione many-to-many)
	@Test
	public void H_testSerieTv() {
		
		//creo una serieTV e la salvo
		SerieTv serie = new SerieTv();
		serie.setTitolo("How I met your mother");
		serie.setGenere("Commedia");
		serie.setIn_corso(false);
		serieTvService.saveEntity(serie);
		
		//scarico la lista di attori per usarla tutta come cast della serie
		List<Attore> listaAttori = (List<Attore>) attoreService.getAllEntitySortedById();
		Set<Attore> setAttori = new HashSet<>(listaAttori);
		System.out.println(setAttori);
		
		//scarico la serie del db per verificare che sia stata salvata, negli assert controllo che il titolo sia quello caricato
		List<SerieTv> listaSerie = (List<SerieTv>) serieTvService.getAllEntitySortedById();
		SerieTv foundSerie = (SerieTv) serieTvService.getEntityById(listaSerie.get(listaSerie.size()-1).getId());
		Long id_serie = foundSerie.getId();
		//per esecuzione di test singolo usare la get qui sotto
		//SerieTv foundSerie = (SerieTv) serieTvService.getEntityById(1L);
		assertNotNull(foundSerie);
		assertEquals(foundSerie.getTitolo(), serie.getTitolo());
		
		//scarico tutta la lista delle serie per verificare che non aumenti dopo l'aggiornamento (in quel caso non farebbe correttamente l'aggiornamento)
		List<SerieTv> allSerieSize = (List<SerieTv>) serieTvService.getAllEntitySortedById();
		
		//aggiorno inserendo il cast e salvo
		foundSerie.setAttori(setAttori);
		serieTvService.saveEntity(foundSerie);
		
		//riscarico la serie cercandola per id e controllo che sia quella che desidero e che la dimensione del cast corrisponda a quella inserita
		SerieTv foundSerie2 = (SerieTv) serieTvService.getEntityById(id_serie);
		//per esecuzione di test singolo usare la get qui sotto
		//SerieTv foundSerie2 = (SerieTv) serieTvService.getEntityById(1L);
		assertNotNull(foundSerie);
		assertEquals(foundSerie.getTitolo(), foundSerie2.getTitolo());
		Iterator<Attore> i = foundSerie.getAttori().iterator();
		Object o;
		while(i.hasNext()) {
			o = i.next();
			System.out.println(o.toString());
		}
		
		//scarico tutte le serie e controllo che siano sempre lo stesso numero anche dopo l'aggiornamento (aggiornamento riuscito,
		// la serie non è stata caricata nel DB come una nuova serie, ma ha aggiornato correttamente)
		List<SerieTv> allSerie = (List<SerieTv>) serieTvService.getAllEntity();
		assertNotNull(allSerie);
		assertEquals(allSerie.size(), allSerieSize.size());
		
		
	}



	@Test
	public void I_testSaveAttore() {
		Attore attore = new Attore();
		attore.setId(4L);
		attore.setNome("nome");
		attoreService.saveEntity(attore);
		Attore trovaAttore = (Attore) attoreService.getEntityById(attore.getId());
		
		assertNotNull(trovaAttore);
		assertEquals(attore.getNome(), trovaAttore.getNome());
		
		attoreService.deleteEntityById(attore.getId());
	}
	
	
	
	//test su episodio e stagione (relazione uno a molti)
	@Test
	public void J_testStagioneEpisodio() {
		//test sulla getEntityById
		Episodio episodio = (Episodio) episodioService.getEntityById(1L);
		assertNotNull(episodio);
		assertEquals(episodio.getTitolo(), "episodio 1");
		
		//creo una stagione e la salvo
		Stagione stagione1 = new Stagione();
		stagione1.setNumero(1);
		stagione1.setAnno_uscita(2020);
		stagione1.setTrama("trama stagione 1");
		stagioneService.saveEntity(stagione1);
		
		//la cerco per id e verifico che esista (test per salvataggio e ricerca per id=
		Stagione foundStagione = (Stagione) stagioneService.getEntityById(1L);
		assertNotNull(foundStagione);
		assertEquals(foundStagione.getAnno_uscita(), 2020);
		//scarico dal DB tutti gli episodi e avvaloro due di loro con una stagione (relazione uno a molti) e salvo(aggiorno) nel DB
		List<Episodio> listEpisodi = (List<Episodio>) episodioService.getAllEntity();
		listEpisodi.get(0).setStagione(foundStagione);
		listEpisodi.get(1).setStagione(foundStagione);
		
		episodioService.saveEntity(listEpisodi.get(0));
		episodioService.saveEntity(listEpisodi.get(1));
		
		
		//prendo un episodio senza stagione e verifico che il valore restituito dal db sia NULL
		episodio = (Episodio) episodioService.getEntityById(3L);
		assertNull(episodio.getStagione());
		//prendo un episodio associato ad una stagione (uno a molti tra stagione ed episodio) e verifico se vi è la relazione con stagione
		episodio = (Episodio) episodioService.getEntityById(1L);
		assertNotNull(episodio.getStagione());
		//verifico con gli id
		assertEquals(episodio.getStagione().getId(), foundStagione.getId());
	}
	
	//test registrazione utente
	@Test
	public void testRegistraUtente() {
		//successo
		Ruolo r = new Ruolo("Utente");
		ruoloService.saveEntity(r);
		RegistrazioneUtenteDto utenteDto1 = new RegistrazioneUtenteDto("NomeTest", "CognomeTest", "Password.1","Password.1", "test@mail.it");
		Utente u = utenteService.save(utenteDto1);
		Utente trovaUtente = (Utente) utenteService.findByEmail(utenteDto1.getEmail());
		assertNotNull(trovaUtente);
		assertEquals(u.getCognome(), trovaUtente.getCognome());
		assertEquals(u.getPassword(), trovaUtente.getPassword());		
		
	}
	
	@Test
	public void testRegistraUtenteFailMail() {
		//fallimento: mail invalida
		Ruolo r = new Ruolo("Utente");
		ruoloService.saveEntity(r);
		RegistrazioneUtenteDto utenteDto1 = new RegistrazioneUtenteDto("NomeTest", "CognomeTest", "Password.1","Password.1", "testmail.it");
		BindingResult risultato = mock(BindingResult.class);
		String esito = registrazioneUtenteController.RegistrazioneAccountUtente(utenteDto1, risultato);
		/*
		 * se il metodo registrazioneUtenteController restituisce la stringa registrazione, significa
		 * che il metodo sta indirizzando l'utente nuovamente alla pagina di registrazione e dunque
		 * si è verificato un errore e l'utente non è stato registrato
		 */
		assertEquals("registrazione", esito);
	}
	
	@Test
	public void testRegistraUtenteFailMatching() {
		//fallimento: le password non coincidono
		Ruolo r = new Ruolo("Utente");
		ruoloService.saveEntity(r);
		RegistrazioneUtenteDto utenteDto1 = new RegistrazioneUtenteDto("NomeTest", "CognomeTest", "Password.11","Password.1", "test@mail.it");
		BindingResult risultato = mock(BindingResult.class);
		String esito = registrazioneUtenteController.RegistrazioneAccountUtente(utenteDto1, risultato);
		assertEquals("registrazione", esito);
	}
	
	@Test
	public void testRegistraUtenteFailAccountEsistente() {
		//successo e fallimento causa account già presente a db
		Ruolo r = new Ruolo("Utente");
		ruoloService.saveEntity(r);
		RegistrazioneUtenteDto utenteDto1 = new RegistrazioneUtenteDto("NomeTest", "CognomeTest", "Password.1","Password.1", "test1@mail.it");
		RegistrazioneUtenteDto utenteDto2 = new RegistrazioneUtenteDto("NomeTest2", "CognomeTest2", "Password.12","Password.12", "test1@mail.it");
		BindingResult risultato = mock(BindingResult.class);
		String esito = registrazioneUtenteController.RegistrazioneAccountUtente(utenteDto1, risultato);
		String esito2 = registrazioneUtenteController.RegistrazioneAccountUtente(utenteDto2, risultato);
		//redirect:/registrazione?success viene restituito in caso di inserimento avvenuto con successo
		assertEquals("redirect:/registrazione?success", esito);
		assertEquals("registrazione", esito2);
	}
	
	@Test
    public void testLoginUtente() {
        Ruolo r = new Ruolo("Utente Premium");
        ruoloService.saveEntity(r);
        RegistrazioneUtenteDto utenteDto1 = new RegistrazioneUtenteDto("NomeTest", "CognomeTest", "Password.1","Password.1", "testlogin1@mail.it");
        Utente u = utenteService.save(utenteDto1);
        //metodo di caricamento dati utenti da mail
        UserDetails ud = utenteService.loadUserByUsername(u.getEmail());
        assertEquals(ud.getUsername(), u.getEmail());
        assertEquals(ud.getAuthorities().toString(), "[Utente Premium]");
    }

    @Test
    public void testLoginUtenteFail() {
        //metodo di caricamento dati utenti da mail
    	try {
    		UserDetails ud = utenteService.loadUserByUsername("testlogin2@mail.it");
    	}
    	catch (Exception e) {
    		assertEquals(e.getMessage(), "Username o password non validi.");
    	}
    }
    
    @Test
    public void testLoginAdmin() {
    	
    	/*nel file data.sql è stato inserito tramite query sql un amministratore, in quanto la registrazione
    	 * di questo tipo di utente non avviene tramite front-end
    	*/
    	UserDetails ud = utenteService.loadUserByUsername("mail@test.it");
        assertEquals(ud.getUsername(), "mail@test.it");
        assertEquals(ud.getAuthorities().toString(), "[Amministratore]");
    }
    
    
    @Test
    public void testLoginAdminFail() {
    	try {
    		UserDetails ud = utenteService.loadUserByUsername("mail@test.it");
    	}
    	catch (Exception e) {
    		assertEquals(e.getMessage(), "Username o password non validi.");
    	}
    }
    
    @Test
    public void testCambioPassword() {
    	//Caso negativo
		Ruolo r = new Ruolo("Utente Premium");
	    ruoloService.saveEntity(r);
	    RegistrazioneUtenteDto utenteDto1 = new RegistrazioneUtenteDto("NomeTest", "CognomeTest", "Password.3","Password.3", "testlogin3@mail.it");
	    utenteService.save(utenteDto1);
    	String emailRegistrata = "testlogin3@mail.it";
    	Principal principalMock = mock(Principal.class);
    	when(principalMock.getName()).thenReturn(emailRegistrata);
    	String esito = utenteController.modificaPassword("Password.2", "nonCorrisponde", "sbagliata", principalMock);
    	assertEquals(esito, "redirect:modificaPassword?notMatching&wrongOld");
    	
    	//Caso negativo
    	esito = utenteController.modificaPassword("Password.2", "Password.2", "Password.3", principalMock);
    	assertEquals(esito, "redirect:paginaPersonale?pswSuccess");
    	
    }
    
    @SuppressWarnings("deprecation")
	@Test
    public void testRipristinaPassword() {
    	//esito positivo: caso mail registrata
    	String emailRegistrata = "testlogin1@mail.it";
    	UserDetails ud = utenteService.loadUserByUsername(emailRegistrata);
    	String passwordPrec = ud.getPassword();
    	String esito = utenteController.inviaEmail(emailRegistrata);
    	assertEquals("redirect:/login?ripristino", esito);
    	
    	ud = utenteService.loadUserByUsername(emailRegistrata);
    	String nuovaPass = ud.getPassword();
    	
    	//controllo che la password sia stata ripristinata
    	assertThat(passwordPrec, not(equalTo(nuovaPass)));
    	ud = utenteService.loadUserByUsername(emailRegistrata);
        assertEquals(ud.getUsername(), emailRegistrata);
        
    	//esito negativo: caso mail non registrata
    	String emailNonRegistrata ="testlogin2@mail.it";
    	esito = utenteController.inviaEmail(emailNonRegistrata);
    	assertEquals("redirect:/passwordSmarrita?error", esito);
    	
    	}
    
    @Test
    public void testCaricamentoUtente() {
    	try {
    		Utente u = utenteService.loadUserInfoByUsername("mail@test.it");
    		assertEquals(u.getEmail(), "mail@test.it");
    	}
    	catch (Exception e) {
    		assertEquals(e.getMessage(), "Username o password non validi.");
    	}
    }
    
    
}
